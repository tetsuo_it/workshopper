package com.cerved.wrp.workshopper.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.cerved.wrp.workshopper.model.Event;

public interface EventRepository extends ReactiveCrudRepository<Event, String> {

}
