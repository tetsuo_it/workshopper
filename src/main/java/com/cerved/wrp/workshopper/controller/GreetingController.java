package com.cerved.wrp.workshopper.controller;

import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cerved.wrp.workshopper.LoggingElement;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/basic")
public class GreetingController extends LoggingElement {

	@GetMapping
	public Mono<String> greeting(@RequestParam(required = false, defaultValue = "") final String name) {
		return Mono.just(Optional.ofNullable(name).map(n -> "Welcome " + n).orElse("Welcome"));
	}

}
