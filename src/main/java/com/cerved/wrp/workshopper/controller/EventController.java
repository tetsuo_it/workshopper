package com.cerved.wrp.workshopper.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cerved.wrp.workshopper.model.Event;
import com.cerved.wrp.workshopper.repository.EventRepository;

@RestController
@RequestMapping("/events")
public class EventController extends BasicCrudController<Event, String> {

	public EventController(final EventRepository repository) {
		super(repository);
	}
}
