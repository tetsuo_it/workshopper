package com.cerved.wrp.workshopper.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cerved.wrp.workshopper.LoggingElement;

import reactor.core.publisher.Mono;

@Controller
public class HomeController extends LoggingElement {

	@RequestMapping("/")
	public Mono<String> home() {
		return Mono.just("index");
	}

}
