package com.cerved.wrp.workshopper.controller;

import javax.validation.Valid;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.cerved.wrp.workshopper.LoggingElement;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class BasicCrudController<T, Y> extends LoggingElement {

	private final ReactiveCrudRepository<T, Y> repository;

	public BasicCrudController(final ReactiveCrudRepository<T, Y> repository) {
		this.repository = repository;
	}

	@GetMapping("/")
	public Flux<T> findAll() {
		return repository.findAll();
	}

	@GetMapping("/{id}")
	public Mono<ResponseEntity<T>> findById(@PathVariable(value = "id") final Y id) {
		return repository.findById(id).map(saved -> ResponseEntity.ok(saved))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@PostMapping("/")
	public Mono<T> create(@RequestBody final T instance) {
		log.info(instance.toString());
		return repository.save(instance);
	}

	@PutMapping("/{id}")
	public Mono<ResponseEntity<T>> update(@PathVariable(value = "id") final Y id,
			@Valid @RequestBody final T instance) {
		return repository.findById(id).flatMap(e -> {
			return repository.save(e);
		}).map(u -> new ResponseEntity<>(u, HttpStatus.OK)).defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> delete(@PathVariable(value = "id") final Y id) {
		return repository.findById(id)
				.flatMap(e -> repository.delete(e).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

}
