package com.cerved.wrp.workshopper.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document
@NoArgsConstructor
public class Event {

	@Id
	private String id;
	private String name;
	private Date date;

	public Event(final String name) {
		super();
		this.name = name;
		date = new Date();
	}

}
