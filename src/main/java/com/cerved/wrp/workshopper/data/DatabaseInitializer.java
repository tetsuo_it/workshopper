package com.cerved.wrp.workshopper.data;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cerved.wrp.workshopper.LoggingElement;
import com.cerved.wrp.workshopper.model.Event;
import com.cerved.wrp.workshopper.repository.EventRepository;

import reactor.core.publisher.Flux;

@Configuration
public class DatabaseInitializer extends LoggingElement {

	@Bean
	CommandLineRunner init(final EventRepository repository) {
		return args -> {
			Flux.just(new Event("Plenaria 2018")).flatMap(repository::save).subscribe(e -> log.info(e.toString()));
		};
	}

}
