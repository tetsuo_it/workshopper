package com.cerved.wrp.workshopper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkshopperApplication {

	public static void main(final String[] args) {
		SpringApplication.run(WorkshopperApplication.class, args);
	}

}
