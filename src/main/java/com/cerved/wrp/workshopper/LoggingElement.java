package com.cerved.wrp.workshopper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingElement {

	/**
	 * The protected reference to the logger.
	 */
	protected final Logger log;

	/**
	 * Standard getter for log.
	 * 
	 * @return the log
	 */
	public Logger getLog() {
		return log;
	}

	/**
	 * Standard constructor.
	 */
	public LoggingElement() {
		log = LoggerFactory.getLogger(getClass().getName());
	}
}
