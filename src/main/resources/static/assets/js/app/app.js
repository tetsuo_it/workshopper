'use strict';

var app = angular.module('wrp',
		[ 'ngCookies', 'ngResource', 'ngRoute', 'ngSanitize', 'ngTouch' ])
		.config(function($routeProvider) {
			$routeProvider.when('/', {
				templateUrl : 'views/welcome.html'
			}).when('/health', {
				controller : 'HealthCtrl',
				templateUrl : 'views/health.html'
			}).when('/events', {
				controller : 'MainCtrl',
				templateUrl : 'views/events.html'
			}).otherwise({
				redirectTo : '/'
			})
		});
