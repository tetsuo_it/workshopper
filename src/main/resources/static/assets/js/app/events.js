'use strict';

angular
	.module('wrp')
	.controller(
		'MainCtrl',
		function($scope, $http) {

			$scope.title = "Eventi";
			$scope.subtitle = "Lista Completa";

			$scope.init = function(id) {
				$http.get('./events/').then(function(response) {
					$scope.events = response.data;
					console.log($scope.events);
				});
			}

			$scope.send = function() {
				// use $.param jQuery function to serialize data from JSON 
				var data = JSON.stringify({
					name : $scope.name,
					date : moment($scope.date,'MM/DD/YYYY')
				});

				var config = {
					headers : {
						'Content-Type' : 'application/json;charset=utf-8;'
					}
				}

				console.log("Request  :"+data);

				$http.post('/events/', data, config)
					.success(function(data, status, headers, config) {
						$scope.event = data;
						$scope.init();
						$scope.$apply;
					})
					.error(function(data, status, header, config) {
						$scope.ResponseDetails = "Data: " + data +
							"<hr />status: " + status +
							"<hr />headers: " + header +
							"<hr />config: " + config;
					});
			};

			$scope.delete = function(id) {
				console.log("Deleting " + id);
				$http.delete('/events/' + id).success(function(data, status) {
					console.log("Deleted " + data);
					$scope.init();
					$scope.$apply;
				});
			}

			$scope.init();
		});