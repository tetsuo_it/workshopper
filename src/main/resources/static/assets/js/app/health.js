'use strict';

angular
	.module('wrp')
	.controller(
		'HealthCtrl',
		function($scope, $http) {

			$scope.title = "Health";
			$scope.subtitle = "Informazioni";

			$scope.init = function(id) {
				$http.get('./application/health').then(function(response) {
					$scope.health = response.data;
					console.log($scope.events);
				});
			}
			
			$scope.init();
		});