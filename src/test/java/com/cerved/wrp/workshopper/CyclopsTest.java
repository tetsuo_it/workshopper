package com.cerved.wrp.workshopper;

import java.util.Arrays;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aol.cyclops.trycatch.Try;

public class CyclopsTest {

	public static final Logger log = LoggerFactory.getLogger(CyclopsTest.class);

	static Function<String, Try<Integer, NumberFormatException>> extract = (s) -> Try
			.catchExceptions(NumberFormatException.class).tryThis(() -> Integer.parseInt(s))
			.onFail(NumberFormatException.class, e -> log.error(e.toString()));

	static Function<String, Try<Long, NumberFormatException>> publish = (s) -> Try
			.catchExceptions(NumberFormatException.class).tryThis(() -> Long.parseLong(s))
			.onFail(NumberFormatException.class, e -> log.error(e.getMessage()));

	static Function<String, Long> f = (s) -> extract.apply(s).map(t -> publish.apply(t.toString())).map(Try::get)
			.orElse(0L);

	public static void main(final String[] args) {
		final Long record = Arrays.asList("1", "2a", "15", "12b", "34").stream().mapToLong(f::apply).sum();
		log.info("Sum:" + record.toString());
	}

}
