package com.cerved.wrp.workshopper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import com.aol.cyclops.trycatch.Try;

public class Operations {

	private <T extends Exception> void err(final T e) {
		System.err.println(e.toString());
	}

	/**
	 * Safely parse a string ant parse it as an Integer.
	 */
	private final Function<String, Try<Integer, NumberFormatException>> parse = (s) -> Try
			.catchExceptions(NumberFormatException.class).tryThis(() -> Integer.parseInt(s))
			.onFail(NumberFormatException.class, this::err);

	/**
	 * Provides an integer between 1 and 10
	 */
	private final Supplier<Integer> n = () -> (int) (Math.random() * 9) + 1;

	/**
	 * Provides a couple of integers numbers between 1 and 10
	 */
	private final Supplier<Integer[]> numbers = () -> new Integer[] { n.get(), n.get() };

	/**
	 * Format the question to be printed on the console.
	 */
	private final BiFunction<Integer, Integer[], String> question = (i, nn) -> String.format("Domanda %d: %dx%d", i,
			nn[0], nn[1]);

	/**
	 * Safely evaluate the integer provided as string with the two provided
	 * integers.
	 */
	private final BiFunction<Integer, Integer[], Boolean> eval = (answer, nn) -> Optional.ofNullable(answer)
			.map(r -> r == nn[0] * nn[1]).orElse(false);

	/**
	 * Safely evaluate the string provided with the two integers provided as
	 * basis of the operation.
	 */
	private final BiFunction<String, Integer[], Boolean> evalString = (answer, nn) -> parse.apply(answer)
			.map(n -> eval.apply(n, nn)).orElse(false);

	/**
	 * Execute the test.
	 */
	private void test() {
		final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		final PrintStream ps = System.out;

		final Integer vote = IntStream.rangeClosed(1, 10).mapToObj(i -> {
			// Creates the stream of operations
			return Optional.of(numbers.get()).map(ns -> {
				ps.println(question.apply(i, ns));
				return ns;
			}).get();
		}).map(o -> {
			// Read from System.in the answers and then evaluates it using the
			// numbers
			return Try.catchExceptions(Exception.class).tryThis(() -> br.readLine()).map(s -> evalString.apply(s, o))
					.orElse(false);
		}).mapToInt(b -> b ? 1 : 0).sum();

		ps.println("Vote:" + vote);
	}

	/**
	 * The main method
	 *
	 * @param args
	 *            The arguments of the main method.
	 */
	public static void main(final String[] args) {
		new Operations().test();
	}

}
